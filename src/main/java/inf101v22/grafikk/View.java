package inf101v22.grafikk;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JComponent;

public class View extends JComponent {
    
    @Override
    public void paint(Graphics g) {
        super.paint(g);

        int margin = 20;
        int componentWidth = this.getWidth();
        int componentHeight = this.getHeight();

        drawChess(g, margin, margin, componentWidth - 2 * margin, componentHeight - 2 * margin);
    }

    private void drawChess(Graphics g, int x, int y, int width, int height) {
        g.setColor(Color.RED);
        g.fillRect(x, y, width, height);

        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                //
                if ((row + col) % 2 == 0) {
                    g.setColor(Color.BLACK);
                }
                else {
                    g.setColor(Color.WHITE);
                }
                int squareX = x + row * width / 8;
                int squareY = y + col * height / 8;
                int nextSquareX = x + (row + 1) * width / 8;
                int nextSquareY = y + (col + 1) * height / 8;
                int squareWidth = nextSquareX - squareX;
                int squareHeigh = nextSquareY - squareY;
                g.fillRect(squareX, squareY, squareWidth, squareHeigh);
            }
        }
    }
}

package inf101v22.arv;

public class Cat extends Mammal {

    Cat(String name) {
        super(name);
    }

    @Override
    String getNoise() {
        return "miaow";
    }
    
}

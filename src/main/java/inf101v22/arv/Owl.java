package inf101v22.arv;

public class Owl extends Mammal {

    Owl(String name) {
        super(name);
    }

    @Override
    void sleep() throws DoesntSleepException {
        throw new DoesntSleepException();
    }

    @Override
    String getNoise() {
        return "ooo";
    }
    
}
